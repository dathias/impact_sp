{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Measuring poverty and inequality with survey data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Overview\n",
    "\n",
    "- Steps and methodological choices in constructing a welfare aggregate\n",
    "- Choosing poverty lines\n",
    "- Overview of different poverty measures\n",
    "- Interactive exercise in R to compute poverty and inequality measures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Learning outcomes\n",
    "\n",
    "By the end of this session, you will be able to:\n",
    "- Recount different approaches to defining and measuring poverty\n",
    "- Apply basic commands in R to calculate the incidence of monetary poverty and examine the welfare distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Steps in measuring poverty\n",
    "\n",
    "1. Define an **indicator of welfare**\n",
    "2. Establish a **poverty line** that indicates the minimum acceptable standard of that indicator\n",
    "3. Generate **summary statistics** to aggregate the information from the distribution  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. Define an indicator of welfare\n",
    "\n",
    "- Analysts tend to use income or consumption measured in monetary units.\n",
    "\n",
    "- Data are usually collected at the household level, so we must make certain assumptions about the distribution of welfare within the household.\n",
    "\n",
    "- Non-monetary indicators are becoming increasingly popular (e.g. multidimensional poverty index)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Income\n",
    "\n",
    "Income is used as the main measure of living standards in most OECD countries and Latin America."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**What is income?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Income\n",
    "\n",
    "- Market income:\n",
    "    - Wages and salaries\n",
    "    - Self-employment net income\n",
    "    - Dividends, interest and other investment income\n",
    "    - Retirement pensions, superannuation and annuities\n",
    "    - Private transfers\n",
    "\n",
    "- Government transfer payments:\n",
    "    - Social protection schemes (old age pensions, child benefits, etc.)\n",
    "    - Other income from government sources"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Income\n",
    "\n",
    "**Pro:**\n",
    "- Easy to measure, given the limited number of sources of income.\n",
    "- Measures degree of household “command” over resources (which they could use if they so wish).\n",
    "- Costs only a fifth as much to collect as expenditure data, so sample can be larger.\n",
    "\n",
    "**Con:**\n",
    "- Likely to be underreported.\n",
    "- May be affected by short-term fluctuations (for example, the seasonal pattern of agriculture).\n",
    "- Some parts of income are hard to observe (for example, informal sector income, home agricultural production, self-employment income).\n",
    "- Link between income and welfare is not always clear.\n",
    "- Reporting period might not capture the “average” income of the household."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Consumption expenditure\n",
    "\n",
    "Consumption usually includes: \n",
    "1. Food consumption\n",
    "2. Non-food items (including health, education and other non-food expenditures)\n",
    "3. Consumer durables\n",
    "4. Housing expenditures (including rent and utilities)\n",
    "\n",
    "Consumption expenditure as a welfare indicator is more commonly used in countries in Africa and Asia.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 1. Food consumption\n",
    "\n",
    "Food consumed **inside the household** from a variety of sources:\n",
    "- Food purchases\n",
    "- Self-produced food\n",
    "- Food received as gifts, remittances and payments in-kind \n",
    "\n",
    "Food consumed **outside the household**:\n",
    "- Restaurants, etc.\n",
    "\n",
    "<br/>\n",
    "\n",
    "Rule of thumb for treatment of **farm households** - Consumption from home production needs to be imputed from:\n",
    "- purchases of such goods by other households\n",
    "- prices collected in a community questionnaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 2. Non-food items\n",
    "\n",
    "- Education (such as tuition fees, textbooks, etc.)\n",
    "- Health (medical care and health expenses)\n",
    "- Clothing and footwear\n",
    "- Transport\n",
    "- Water, electricity, gas and other fuels\n",
    "- Recreation\n",
    "- Personal care\n",
    "- Other miscellaneous goods and services\n",
    "\n",
    "<br/>\n",
    "\n",
    "A choice has to be made in terms of the items to include. Some rules of thumb:\n",
    "- Include education expenditures\n",
    "- Include health expenditures only if they have high expenditure elasticity\n",
    "- Exclude taxes paid, levies, and repayment of loans\n",
    "- Exclude lumpy expenditures such as marriages and dowries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 3. Durable goods\n",
    "\n",
    "- Vehicles\n",
    "- Washing machines\n",
    "- Computers\n",
    "- Etc.\n",
    "\n",
    "<br/>\n",
    "\n",
    "Because durable goods last for several years, and because it is not the purchase of durables that is the relevant component of welfare, they require special treatment when calculating total expenditure.\n",
    "\n",
    "What should be computed is not the expenditure itself but the flow of services that they yield during the reference period. This means that information is needed on the age of each durable good as well as on its original and current value.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 4. Housing expenditures\n",
    "\n",
    "- Actual rent or rental equivalence value\n",
    "- House repair\n",
    "- Decoration\n",
    "- Etc.\n",
    "\n",
    "<br/>\n",
    "\n",
    "Rents can sometimes be observed directly (i.e. households that rent their dwelling). There are two main approaches to deal with households that do not report rents (i.e. owner occupiers):\n",
    "- Respondents can be asked to provide an estimate (or rental equivalent)\n",
    "- Analysts can predict rental payments through the use of imputation models\n",
    "\n",
    "Both procedures, however, work well only where an active rental market is in place."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Consumption expenditure\n",
    "\n",
    "**Pro:**\n",
    "- Shows current actual material standard of living.\n",
    "- Smoothes out irregularities, and so reflects long-term average well-being.\n",
    "- Less understated than income, because expenditure is easier to recall.\n",
    "\n",
    "**Con:**\n",
    "- Households may not be able to smooth consumption (for example, via borrowing, social networks).\n",
    "- Consumption choices made by households may be misleading (for example, if a rich household chooses to live simply, that does not mean it is poor).\n",
    "- Some expenses are not incurred regularly, so data may be noisy.\n",
    "- Difficult to measure some components of consumption, including durable goods."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adjusting for differences in cost of living\n",
    "\n",
    "\n",
    "**Spatial price variation**: People who live in differenty parts of the country pay different prices for comparable goods\n",
    "- Differences tend to be small in countries with integrated distribution systems, where transportation is easy and inexpensive. \n",
    "- Differences can be large in many developing countries.\n",
    "\n",
    "Common solution:\n",
    "- Deflate nominal aggregate expenditures by a *Paasche price index*, which takes into account prices of goods and services and households' expenditure patterns.\n",
    "\n",
    "Sources of price data:\n",
    "- Reports of purchases by households in the survey itself.\n",
    "- Dedicated price questionnaire, often administed as part of a community questionnaire.\n",
    "- Ancillary data, for example from government price surveys.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adjusting for differences in cost of living\n",
    "\n",
    "\n",
    "**Temporal price variation**: General price levels change over time (inflation). This affects comparisons of households:\n",
    "- Within a cross-sectional survey if data is collected over a long period of time (e.g. one year).\n",
    "- Between two surveys for the same country that are some years apart.\n",
    "\n",
    "Common solution: \n",
    "- Use national consumer price index to create a temporal index.\n",
    "- Deflate nominal expenditures to ensure that welfare comparisons between two periods are not driven by inflation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adjusting for differences in household composition\n",
    "\n",
    "- Equivalence scales are deflators that are used to convert real household expenditure or income into measures of **individual welfare**.\n",
    "\n",
    "\n",
    "- Most common approach is to divide by household size, i.e. **per capita** expenditure or income.\n",
    "\n",
    "\n",
    "- Others approaches take into account that different individuals may have different needs or that larger households enjoy certain economies of scale.\n",
    "    - OECD equivalent scale\n",
    "    - OECD modified scale\n",
    "    - Square root scale\n",
    "    - Etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adjusting for differences in household composition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table>\n",
       "<caption>A data.frame: 6 × 7</caption>\n",
       "<thead>\n",
       "\t<tr><th scope=col>X</th><th scope=col>UqNr</th><th scope=col>PersonNR</th><th scope=col>Gender</th><th scope=col>Age</th><th scope=col>totmhinc</th><th scope=col>house_wgt</th></tr>\n",
       "\t<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>\n",
       "</thead>\n",
       "<tbody>\n",
       "\t<tr><td>1</td><td>1.601007e+17</td><td>1</td><td>1</td><td>50</td><td>3400</td><td> 791.6884</td></tr>\n",
       "\t<tr><td>2</td><td>1.601007e+17</td><td>1</td><td>1</td><td>29</td><td>2000</td><td>1661.8212</td></tr>\n",
       "\t<tr><td>3</td><td>1.601007e+17</td><td>2</td><td>2</td><td>24</td><td>2000</td><td>1661.8212</td></tr>\n",
       "\t<tr><td>4</td><td>1.601007e+17</td><td>3</td><td>2</td><td> 0</td><td>2000</td><td>1661.8212</td></tr>\n",
       "\t<tr><td>5</td><td>1.601012e+17</td><td>1</td><td>2</td><td>70</td><td>4140</td><td> 602.0755</td></tr>\n",
       "\t<tr><td>6</td><td>1.601012e+17</td><td>2</td><td>2</td><td>48</td><td>4140</td><td> 602.0755</td></tr>\n",
       "</tbody>\n",
       "</table>\n"
      ],
      "text/latex": [
       "A data.frame: 6 × 7\n",
       "\\begin{tabular}{r|lllllll}\n",
       " X & UqNr & PersonNR & Gender & Age & totmhinc & house\\_wgt\\\\\n",
       " <int> & <dbl> & <int> & <int> & <int> & <int> & <dbl>\\\\\n",
       "\\hline\n",
       "\t 1 & 1.601007e+17 & 1 & 1 & 50 & 3400 &  791.6884\\\\\n",
       "\t 2 & 1.601007e+17 & 1 & 1 & 29 & 2000 & 1661.8212\\\\\n",
       "\t 3 & 1.601007e+17 & 2 & 2 & 24 & 2000 & 1661.8212\\\\\n",
       "\t 4 & 1.601007e+17 & 3 & 2 &  0 & 2000 & 1661.8212\\\\\n",
       "\t 5 & 1.601012e+17 & 1 & 2 & 70 & 4140 &  602.0755\\\\\n",
       "\t 6 & 1.601012e+17 & 2 & 2 & 48 & 4140 &  602.0755\\\\\n",
       "\\end{tabular}\n"
      ],
      "text/markdown": [
       "\n",
       "A data.frame: 6 × 7\n",
       "\n",
       "| X &lt;int&gt; | UqNr &lt;dbl&gt; | PersonNR &lt;int&gt; | Gender &lt;int&gt; | Age &lt;int&gt; | totmhinc &lt;int&gt; | house_wgt &lt;dbl&gt; |\n",
       "|---|---|---|---|---|---|---|\n",
       "| 1 | 1.601007e+17 | 1 | 1 | 50 | 3400 |  791.6884 |\n",
       "| 2 | 1.601007e+17 | 1 | 1 | 29 | 2000 | 1661.8212 |\n",
       "| 3 | 1.601007e+17 | 2 | 2 | 24 | 2000 | 1661.8212 |\n",
       "| 4 | 1.601007e+17 | 3 | 2 |  0 | 2000 | 1661.8212 |\n",
       "| 5 | 1.601012e+17 | 1 | 2 | 70 | 4140 |  602.0755 |\n",
       "| 6 | 1.601012e+17 | 2 | 2 | 48 | 4140 |  602.0755 |\n",
       "\n"
      ],
      "text/plain": [
       "  X UqNr         PersonNR Gender Age totmhinc house_wgt\n",
       "1 1 1.601007e+17 1        1      50  3400      791.6884\n",
       "2 2 1.601007e+17 1        1      29  2000     1661.8212\n",
       "3 3 1.601007e+17 2        2      24  2000     1661.8212\n",
       "4 4 1.601007e+17 3        2       0  2000     1661.8212\n",
       "5 5 1.601012e+17 1        2      70  4140      602.0755\n",
       "6 6 1.601012e+17 2        2      48  4140      602.0755"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Import the dataset\n",
    "data <- read.csv('/Users/bjorn/OneDrive - Development Pathways Ltd/Projects/ILO Turin/Sessions/ghs2017.csv')\n",
    "\n",
    "\n",
    "# View first rows\n",
    "head(data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "library(dplyr)\n",
    "# Create new column (variable) with the household size\n",
    "data <- data %>% group_by(UqNr) %>% add_tally(name = 'hhsize')\n",
    "\n",
    "# Create binary markers for children and adults\n",
    "data$child <- 1 * (data$Age < 14)\n",
    "data$adult <- 1 * (data$Age >= 14)\n",
    "\n",
    "# Create new columns (variables) with number of children and adults\n",
    "data <- data %>% group_by(UqNr) %>% add_tally(child, name = 'nchild')\n",
    "data <- data %>% group_by(UqNr) %>% add_tally(adult, name = 'nadult')\n",
    "\n",
    "# View dataframe\n",
    "head(data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Calculate household income per capita\n",
    "\n",
    "data$income_percapita <- data$totmhinc / data$hhsize\n",
    "\n",
    "# Calculate OECD equivalence scale\n",
    "\n",
    "data$income_oecd <- data$totmhinc / (1 + 0.7 * (data$nadult - 1) + 0.5 * (data$nchild))\n",
    "\n",
    "# Calculate OECD modified equivalence scale\n",
    "\n",
    "data$income_modoecd <- data$totmhinc / (1 + 0.5 * (data$nadult - 1) + 0.3 * (data$nchild))\n",
    "\n",
    "# Show first rows of dataframe\n",
    "head(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Further reading\n",
    "\n",
    "For more information, see: \n",
    "\n",
    "- Deaton, Angus & Zaidi, Salman (2002). *Guidelines for Constructing Consumption Aggregates for Welfare Analysis.* LSMS Working Paper No. 135. Washington, DC: World Bank. URL: http://hdl.handle.net/10986/14101\n",
    "\n",
    "For an introductory example with syntax, see:\n",
    "\n",
    "- Construction of Consumption Aggregates for the Ethiopia Socioeconomic Survey (Wave 1). URL: https://siteresources.worldbank.org/INTLSMS/Resources/3358986-1233781970982/5800988-1367841456879/9170025-1367841502220/ESS1_consumption_aggregate_documentation.pdf\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## 1. Define an indicator of welfare\n",
    "\n",
    "\n",
    "Other potential candidates include, among others:\n",
    "\n",
    "- Asset wealth index: https://www.dhsprogram.com/topics/wealth-index/Wealth-Index-Construction.cfm\n",
    "\n",
    "- Multidimensional poverty index: https://ophi.org.uk/publications/mpi-methodological-notes/\n",
    "\n",
    "- Multidimensional child poverty (MODA): https://www.unicef-irc.org/research/multidimensional-child-poverty/\n",
    "\n",
    "- Indicators related to food and nutrition\n",
    "\n",
    "- Etc.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise\n",
    "\n",
    "Review the questionnaire from the 2010 Vietnam Household Living Standard Surveys (VHLSS):\n",
    "https://drive.google.com/file/d/0B5Kjg1b9s7JRT0I0VnBOcVpoQ2c/view?usp=sharing\n",
    "\n",
    "\n",
    "Group 1:\n",
    "- The questionnaire collects data on which components of **income**? What is the reference period for each component?\n",
    "\n",
    "Group 2:\n",
    "- The questionnaire collects data on which components of **consumption expenditure**? What is the reference period for each component?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2. Establish a poverty line\n",
    "\n",
    "\n",
    "<br/>\n",
    "\n",
    "How do we set a threshold to distinguish between those classified as 'poor' and those classified as non-poor?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Relative poverty lines\n",
    "\n",
    "\n",
    "Many countries, especially those in Europe and other OECD countries, set poverty lines based on relative standards. \n",
    "\n",
    "The underlying belief is that deprivations should be judged **relative to the well-being of the majority of society**.\n",
    "\n",
    "Relative poverty can be reduced but never fully eliminated (except if income equality is fully achieved).\n",
    "\n",
    "Examples:\n",
    "- Share of mean income or expenditure\n",
    "- Share of median income or expenditure\n",
    "- Specified percentage of the distribution (e.g. bottom 20%)\n",
    "- Etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Absolute poverty lines\n",
    "\n",
    "Deprivation in an absolute sense, i.e. the value of a set of resources deemed necessary to maintain a minimal standard of well-being.\n",
    "\n",
    "Absolute poverty **thresholds are fixed** and represent the same purchasing power year after year.\n",
    "\n",
    "Common approaches:\n",
    "- Thresholds computed using basic need approach\n",
    "- International poverty lines from World Bank"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Cost of basic needs approach\n",
    "\n",
    "- Identify a **basket of foods** that will deliver the minimal nutritional requirements\n",
    "    - Example: minimum threshold of 2,100 calories per person per day\n",
    "    - Adjustments can be made for age, gender and location\n",
    "    \n",
    "\n",
    "- Estimate the **cost** for meeting this food energy requirement, using a diet that reflects the habits of those near the poverty line.\n",
    "    - Example: diets of those in the lowest quintile\n",
    "    - Challenging if diets vary widely across the country\n",
    "    \n",
    "    \n",
    "- Add a **nonfood** component\n",
    "    - Example: minimum cost for housing\n",
    "    - No standard approaches for nonfood component of poverty line\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## International poverty lines\n",
    "\n",
    "Expressed using dollars in purchasing power parity (PPP) = Rates of currency conversion that try to equalise the purchasing power of different currencies, by eliminating the differences in price levels between countries.\n",
    "\n",
    "- PPP \\$1.9 per person per day for **low-income** countries\n",
    "- PPP \\$3.2 per person per day for **lower middle-income** countries\n",
    "- PPP \\$5.5 per person per day for **upper middle-income** countries\n",
    "- PPP \\$21.7 per person per day for **high income** countries\n",
    "\n",
    "For a list of countries by income groupings, see:\\\n",
    "https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# https://tinyurl.com/y634tr5r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Converting PPP dollars into local currency\n",
    "\n",
    "Find the PPP conversion rate for private consumption (LCU per international dollar) for your country:\n",
    "https://data.worldbank.org/indicator/PA.NUS.PRVT.PP\n",
    "\n",
    "Calculate the value of PPP\\$ 3.2 in your local currency.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Example: Italy (EUR)\n",
    "\n",
    "# PPP conversion factor\n",
    "ppp = 0.784\n",
    "\n",
    "# Define international poverty line\n",
    "povline_low = 1.9 * ppp\n",
    "povline_lmiddle = 3.2 * ppp\n",
    "povline_umiddle = 5.5 * ppp\n",
    "povline_high = 21.7 * ppp\n",
    "\n",
    "\n",
    "# Print values\n",
    "povline_low\n",
    "povline_lmiddle\n",
    "povline_umiddle\n",
    "povline_high"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Convert daily to monthly poverty line\n",
    "povline_high_monthly = povline_high * 365 / 12\n",
    "\n",
    "# Print monthly value\n",
    "povline_high_monthly"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## A word of caution\n",
    "\n",
    "- All poverty lines are somewhat **arbitrary**. There is no single 'objective' poverty line. Even in the basic needs approach, analysts have to make choices and assumptions.\n",
    "\n",
    "\n",
    "- Poverty measures tend to be **highly sensitive** to the choice of poverty line.\n",
    "\n",
    "\n",
    "- Difference between concepts and reality: A poverty line has little empirical correspondence in the daily lives of households. Nothing 'special' happens when household cross the poverty line.\n",
    "\n",
    "\n",
    "- Rather than fixating on a single poverty line, it is important to look at the entire distributions of income or expenditure and make comparisons across a wide range of poverty thresholds to get a more complete and robust picture."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center><img src='../presentations/gfx/povrates.png'></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center><img src='../presentations/gfx/dynamics.png'></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center><img src='../presentations/gfx/alluvial.png'></center>"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
